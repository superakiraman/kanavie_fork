package ktc.kanavie.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = new Intent(getApplicationContext(), GuideActivity.class)
				.putExtra("startLatitude", 36.578144)
				.putExtra("startLongtitude", 136.64889)
				.putExtra("bTime", 500)
				.putExtra("goalDetail", "サンプル");

		startActivity(intent);
		finish();
	}
}
