package ktc.kanavie.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import ktc.kanavie.BusDate;
import ktc.kanavie.BusStop;
import ktc.kanavie.EarthLocation;
import ktc.kanavie.LocationUtilities;
import ktc.kanavie.R;
import ktc.kanavie.db.BusRouteHolder;
import ktc.kanavie.db.BusRouteHolder.BusRoute;
import ktc.kanavie.db.BusStopHolder;
import ktc.kanavie.db.DiagramHolder;
import ktc.kanavie.db.DiagramHolder.BusPlan;
import ktc.kanavie.db.DiagramHolder.BusTimeData;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class GuideActivity extends Activity {

	private static final int MAX_WALK_LENGTH = 800;

	private Context appContext = null;
	private Handler selfhandler = null;

	private EarthLocation startPoint = null;
	private EarthLocation goalPoint = null;
	private String goalDetail = "";

	private BusDate baseTime = null;

	private RouteResultAdapter routeAdapter = null;
	private List<RouteElement> elements = null;
	private SparseArray<RouteByElement> byElements = null;

	private BusPlan[] plans = null;

	private ProgressDialog progressDialog = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.kanavie_route);

		appContext = this.getApplicationContext();
		selfhandler = new Handler();

		routeAdapter = new RouteResultAdapter(getApplicationContext());
		elements = new ArrayList<RouteElement>();
		byElements = new SparseArray<RouteByElement>();

		SharedPreferences dataPref =
				getSharedPreferences("UpdateSetting", Context.MODE_PRIVATE);

		if (!dataPref.getBoolean("init", false)) {
			initilizeData();
		} else {
			init();
		}
	}

	private void init() {
		double startLat = 36.561273;
		double startLon = 136.656241;

		double goalLat = 36.560859;
		double goalLon = 136.65814;

		baseTime = new BusDate();

		// 引数読み込み
		Bundle extra = getIntent().getExtras();
		if (extra != null) {
			startLat = extra.getDouble("startLatitude", startLat);
			startLon = extra.getDouble("startLongtitude", startLon);

			goalLat = extra.getDouble("goalLatitude", goalLat);
			goalLon = extra.getDouble("goalLongtitude", goalLon);

			goalDetail = extra.getString("goalDetail");
			if (goalDetail == null) {
				goalDetail = "";
			}

			int time = extra.getInt("bTime", 0);
			if (time == 0) {
				baseTime = new BusDate();
			} else {
				baseTime = new BusDate(time);
			}
		}

		startPoint = new EarthLocation(startLat, startLon);
		goalPoint = new EarthLocation(goalLat, goalLon);

		double gap = LocationUtilities.calcDistanceDifference(startPoint, goalPoint);

		// ルート生成
		if (gap < MAX_WALK_LENGTH) {
			initWalkingOnlyRoute();
		} else {
			initSimpleRoute();
		}

		// 表示設定
		for (RouteElement e : elements) {
			routeAdapter.addElement(e);
		}

		// ListView設定
		ListView main = (ListView)findViewById(R.id.kanavie_result_listview);
		main.setAdapter(routeAdapter);
		main.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (arg3 >= 0) {
					RouteByElement item = byElements.get((int)arg3);
					if (item instanceof RouteByWalk) {
						callMap(((RouteByWalk)item).getStart(), ((RouteByWalk)item).getGoal());
					}
				}
			}
		});
	}

	private void initWalkingOnlyRoute() {
		// 表示生成
		double gap = LocationUtilities.calcDistanceDifference(startPoint, goalPoint);
		int requiredTime = (int)Math.ceil(gap / 80);

		RouteSpot start = new RouteSpot("現在地", "");
		RouteByWalk byWalk = new RouteByWalk(0, requiredTime, startPoint, goalPoint);
		RouteSpot goal = new RouteSpot("目的地", goalDetail);

		elements.add(start);
		elements.add(byWalk);
		elements.add(goal);

		byElements.put(byWalk.getRouteId(), byWalk);
	}

	private void initSimpleRoute() {
		BusPlan mainPlan = LocationUtilities.searchBusRoute(getApplicationContext(), startPoint, goalPoint, baseTime);

		if (mainPlan == null) {
			// 徒歩で
			initWalkingOnlyRoute();
		} else {
			// 用意
			BusRouteHolder brh = new BusRouteHolder(getApplicationContext());
			BusStopHolder bsh = new BusStopHolder(getApplicationContext());

			// 変数
			BusStop busStart = bsh.get(mainPlan.getStart().getBusStopID());
			BusStop busGoal = bsh.get(mainPlan.getGoal().getBusStopID());

			// 表示生成
			double gap = LocationUtilities.calcDistanceDifference(startPoint, busStart.getLocation());
			int requiredTime = (int)Math.ceil(gap / 80);

			RouteSpot start = new RouteSpot("現在地", "");
			RouteByWalk byWalk1 = new RouteByWalk(0, requiredTime, startPoint, busStart.getLocation());
			RouteSpot bus1 = new RouteSpot(busStart.getName(), busStart.getCompany());

			RouteBy byBus = new RouteBy(1,
					brh.get(mainPlan.getRouteId()).getRouteName(),
					busStart.getCompany(),
					mainPlan.getStart().getBusTime(), mainPlan.getGoal().getBusTime());

			RouteSpot bus2 = new RouteSpot(busGoal.getName(), busGoal.getCompany());

			gap = LocationUtilities.calcDistanceDifference(busGoal.getLocation(), goalPoint);
			requiredTime = (int)Math.ceil(gap / 80);

			RouteByWalk byWalk2 = new RouteByWalk(2, requiredTime, busGoal.getLocation(), goalPoint);
			RouteSpot goal = new RouteSpot("目的地", goalDetail);

			elements.add(start);
			elements.add(byWalk1);
			elements.add(bus1);
			elements.add(byBus);
			elements.add(bus2);
			elements.add(byWalk2);
			elements.add(goal);

			byElements.put(byWalk1.getRouteId(), byWalk1);
			byElements.put(byBus.getRouteId(), byBus);
			byElements.put(byWalk2.getRouteId(), byWalk2);
		}
	}

	private void initilizeData() {

		progressDialog = new ProgressDialog(this);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setCancelable(false);
		progressDialog.setTitle("処理中");
		progressDialog.setMessage("データの初期化中です");

		progressDialog.show();

		final Resources res = this.getResources();

		new Thread(new Runnable() {

			@Override
			public void run() {

				try {
					StringBuffer string = new StringBuffer();
					BufferedReader breader = null;
					String[] data = null;

					string = new StringBuffer();
					breader = new BufferedReader(new InputStreamReader(res.openRawResource(R.raw.busstop), "Shift-JIS"));
					while (breader.ready()) {
						string.append(breader.readLine() + "\n");
					}
					breader.close();

					ArrayList<BusStop> busstops = new ArrayList<BusStop>();
					data = string.toString().replace("\r", "").split("\n");
					for (String dat : data) {
						String[] sub = dat.split(",");
						busstops.add(new BusStop(Integer.parseInt(sub[0]), sub[1], sub[4], Double.parseDouble(sub[2]), Double.parseDouble(sub[3])));
					}

					BusStopHolder bh = new BusStopHolder(appContext);
					bh.initialize(busstops.toArray(new BusStop[busstops.size()]));

					BusRouteHolder brh = new BusRouteHolder(appContext);
					ArrayList<BusRoute> br = new ArrayList<BusRouteHolder.BusRoute>();

					string = new StringBuffer();
					breader = new BufferedReader(new InputStreamReader(res.openRawResource(R.raw.busroute), "Shift-JIS"));
					while (breader.ready()) {
						string.append(breader.readLine() + "\n");
					}
					breader.close();

					data = string.toString().replace("\r", "").split("\n");
					for (String dat : data) {
						String[] sub = dat.split(",");

						int[] stop = new int[sub.length - 2];
						for (int i = 2; i < sub.length; i++) {
							stop[i - 2] = Integer.parseInt(sub[i]);
						}

						br.add(brh.new BusRoute(Integer.parseInt(sub[0]), stop, sub[1]));
					}

					brh.initialize(br.toArray(new BusRoute[br.size()]));

					string = new StringBuffer();
					breader = new BufferedReader(new InputStreamReader(res.openRawResource(R.raw.bustime), "Shift-JIS"));
					while (breader.ready()) {
						string.append(breader.readLine() + "\n");
					}
					breader.close();
					data = string.toString().replace("\r", "").split("\n");

					DiagramHolder bth = new DiagramHolder(appContext);
					ArrayList<BusTimeData> data2 = new ArrayList<BusTimeData>();
					for (String dat : data) {
						String[] sub = dat.split(",");
						ArrayList<BusDate> bd = new ArrayList<BusDate>();
						for (int i = 3; i < sub.length; i++) {
							if (sub[i].length() > 3) {
								bd.add(new BusDate(sub[i]));
							}
						}

						data2.add(bth.new BusTimeData(
								Integer.parseInt(sub[0]),
								Integer.parseInt(sub[1]),
								Integer.parseInt(sub[2]),
								bd.toArray(new BusDate[0])));
					}

					bth.initialize(data2.toArray(new BusTimeData[data2.size()]));

					selfhandler.post(new Runnable() {
						@Override
						public void run() {
							progressDialog.dismiss();

							SharedPreferences dataPref =
									getSharedPreferences("UpdateSetting", Context.MODE_PRIVATE);

							dataPref.edit().putBoolean("init", true).commit();

							init();
						}
					});
				} catch (IOException e) {
				}
			}
		}).start();
	}

	private void callMap(EarthLocation location1, EarthLocation location2) {
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
		intent.setData(Uri.parse("http://maps.google.com/maps?saddr=" + location1.toString() + "&daddr=" + location2.toString() + "&dirflg=w"));
		startActivity(intent);
	}

	class RouteResultAdapter extends BaseAdapter {

		private List<RouteElement> elements = null;
		private List<View> viewElements = null;

		private LayoutInflater layoutInflater = null;

		public RouteResultAdapter(Context context) {
			elements = new ArrayList<RouteElement>();
			viewElements = new ArrayList<View>();

			layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public void addElement(RouteElement element) {
			View view = null;

			if (element instanceof RouteSpot) {
				RouteSpot spot = (RouteSpot)element;

				view = layoutInflater.inflate(R.layout.kanavie_route_view_spot, null);

				TextView title = (TextView)view.findViewById(R.id.kanavie_spotname);
				TextView detail = (TextView)view.findViewById(R.id.kanavie_spotdetail);

				title.setText(spot.getSpotTitle());
				detail.setText(spot.getSpotDetail());
			} else if (element instanceof RouteBy) {
				RouteBy by = (RouteBy)element;

				view = layoutInflater.inflate(R.layout.kanavie_route_view_by, null);

				TextView title = (TextView)view.findViewById(R.id.kanavie_methodname);
				TextView detail = (TextView)view.findViewById(R.id.kanavie_useline);
				TextView begin = (TextView)view.findViewById(R.id.kanavie_leavetime);
				TextView end = (TextView)view.findViewById(R.id.kanavie_arrivetime);

				title.setText(by.getMethodTitle());
				detail.setText(by.getMethodDetail());

				begin.setText(by.getBeginTime().toString());
				end.setText(by.getEndTime().toString());
			} else if (element instanceof RouteByWalk) {
				RouteByWalk walk = (RouteByWalk)element;

				view = layoutInflater.inflate(R.layout.kanavie_route_view_bywalk, null);

				TextView requiredTime = (TextView)view.findViewById(R.id.kanavie_requiredtime);

				requiredTime.setText(String.format("約 %s分", walk.getRequiredTime()));
			}

			elements.add(element);
			viewElements.add(view);
		}

		@Override
		public int getCount() {
			return elements.size();
		}

		@Override
		public Object getItem(int position) {
			return elements.get(position);
		}

		@Override
		public long getItemId(int position) {
			RouteElement item = elements.get(position);

			if (item instanceof RouteByElement) {
				return ((RouteByElement)item).getRouteId();
			} else {
				return -1;
			}
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return viewElements.get(position);
		}

	}

	interface RouteElement {
	}

	interface RouteByElement {
		public int getRouteId();
	}

	class RouteSpot implements RouteElement {

		private String spotTitle = "";
		private String spotDetail = "";

		public RouteSpot(String spotTitle, String spotDetail) {
			this.spotTitle = spotTitle;
			this.spotDetail = spotDetail;
		}

		public String getSpotTitle() {
			return spotTitle;
		}

		public String getSpotDetail() {
			return spotDetail;
		}

	}

	class RouteBy implements RouteElement, RouteByElement {

		private int routeId = -1;

		private String methodTitle = "";
		private String methodDetail = "";

		private BusDate beginTime = null;
		private BusDate endTime = null;

		public RouteBy(int routeId, String methodTitle, String methodDetail,
				BusDate beginTime, BusDate endTime) {
			this.routeId = routeId;

			this.methodTitle = methodTitle;
			this.methodDetail = methodDetail;

			this.beginTime = beginTime;
			this.endTime = endTime;
		}

		public int getRouteId() {
			return routeId;
		}

		public String getMethodTitle() {
			return methodTitle;
		}

		public String getMethodDetail() {
			return methodDetail;
		}

		public BusDate getBeginTime() {
			return beginTime;
		}

		public BusDate getEndTime() {
			return endTime;
		}
	}

	class RouteByWalk implements RouteElement, RouteByElement {
		int routeId = -1;
		private int requiredTime = 0;

		private EarthLocation start = null;
		private EarthLocation goal = null;

		public RouteByWalk(int routeId, int requiredTime, EarthLocation start,
				EarthLocation goal) {
			this.routeId = routeId;
			this.requiredTime = requiredTime;

			this.start = start;
			this.goal = goal;
		}

		public int getRouteId() {
			return routeId;
		}

		public int getRequiredTime() {
			return requiredTime;
		}

		public EarthLocation getStart() {
			return start;
		}

		public EarthLocation getGoal() {
			return goal;
		}
	}

}
