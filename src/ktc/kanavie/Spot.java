package ktc.kanavie;

import java.io.Serializable;


public abstract class Spot implements Serializable {

	private static final long serialVersionUID = -7514619773378977441L;

	public abstract int getId();
	public abstract String getName();
	public abstract EarthLocation getLocation();
	public abstract int getStandardBusStopId();
}
