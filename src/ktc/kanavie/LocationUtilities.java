package ktc.kanavie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import ktc.kanavie.db.BusRouteHolder;
import ktc.kanavie.db.BusRouteHolder.BusRoute;
import ktc.kanavie.db.BusStopHolder;
import ktc.kanavie.db.DiagramHolder;
import ktc.kanavie.db.DiagramHolder.BusPlan;
import android.content.Context;

public final class LocationUtilities {

	public static double degreeToRadian(double degree) {
		return degree * Math.PI / 180;
	}

	public static double calcDistanceDifference(double alat, double alng, double blat, double blng) {
		return calcDistanceDifference(new EarthLocation(alat, alng), new EarthLocation(blat, blng));
	}

	/**
	 * メートル単位で距離を返す
	 */
	public static double calcDistanceDifference(EarthLocation a, EarthLocation b) {
		// 厳密には計算しない
		double dy = degreeToRadian((a.getLatitude() - b.getLatitude())) * 6378137;
		double dx = degreeToRadian((a.getLongtitude() - b.getLongtitude())) * 6378137 * Math.cos(degreeToRadian(a.getLatitude()));

		return Math.sqrt(dy * dy + dx * dx);
	}

	public static BusStop searchNearestBusStop(final Context context, final EarthLocation loc) {
		BusStop[] busstops = new BusStopHolder(context).getAll();

		BusStop nearBusstop = busstops[0];
		double distance = LocationUtilities.calcDistanceDifference(loc, nearBusstop.getLocation());
		for (BusStop bs : busstops) {
			double d = LocationUtilities.calcDistanceDifference(loc, bs.getLocation());
			if (d < distance) {
				distance = d;
				nearBusstop = bs;
			}
		}

		return nearBusstop;

	}

	public static BusStop[] searchNearBusStop(final Context context, final EarthLocation loc) {
		BusStop[] busstops = new BusStopHolder(context).getAll();

		Arrays.sort(busstops, new Comparator<BusStop>() {
			@Override
			public int compare(BusStop lhs, BusStop rhs) {
				return (int)(LocationUtilities.calcDistanceDifference(loc, lhs.getLocation())
				- LocationUtilities.calcDistanceDifference(loc, rhs.getLocation()));
			}
		});

		return busstops;
	}

	// 非効率・不正確
	// TODO もっと正確な実装が必要
	public static BusPlan searchBusRoute(final Context context, final EarthLocation start, final EarthLocation goal, final BusDate baseTime) {
		int count = 5;

		BusStop[] stops = searchNearBusStop(context, goal);

		List<BusPlan> plans = new ArrayList<BusPlan>();
		// 近い順から5カ所
		for (int i = 0; i < count; i++) {
			plans.add(searchBusRoute(context, stops[i], start, baseTime));
		}

		if (plans.size() > 0) {
			BusStopHolder bsh = new BusStopHolder(context);

			int index = 0;
			double min = Double.MAX_VALUE;
			for (int i = 0; i < plans.size(); i++) {
				BusStop b = bsh.get(plans.get(i).getStart().getBusStopID());
				double distance = LocationUtilities.calcDistanceDifference(b.getLocation(), start);

				if (distance < min) {
					index = i;
					min = distance;
				}
			}

			return plans.get(index);
		} else {
			return null;
		}

	}

	private static BusPlan searchBusRoute(final Context context, final BusStop goalBusStop, final EarthLocation start, final BusDate baseTime) {
		// 終点のバス停
		BusStop busStopGoal = goalBusStop;

		BusStopHolder bsh = new BusStopHolder(context);
		BusRouteHolder brh = new BusRouteHolder(context);
		DiagramHolder dh = new DiagramHolder(context);

		// goalが含まれるルートを列挙
		BusRoute[] routes = brh.getRouteFromBusStop(busStopGoal.getId());

		// 各ルートでgoalよりも先のstartに最も近いバス停を列挙
		double[] busStopNearStartDistance = new double[routes.length];
		BusStop[] busStopNearStart = new BusStop[routes.length];
		for (int i = 0; i < routes.length; i++) {
			int[] busStops = routes[i].getBusStopList();

			BusStop busStop = null;
			double min = Double.MAX_VALUE;
			for (int j = 0; j < busStops.length; j++) {
				// goalまできたらやめる
				if (busStops[j] == busStopGoal.getId()) {
					break;
				}

				BusStop bs = bsh.get(busStops[j]);
				double distance = calcDistanceDifference(bs.getLocation(), start);

				if (distance < min) {
					busStop = bs;
					min = distance;
				}
			}

			busStopNearStart[i] = busStop;
			busStopNearStartDistance[i] = min;
		}

		// startに最も近いバス停を含むルートを抽出
		int index = 0;
		double min = busStopNearStartDistance[0];
		for (int i = 1; i < busStopNearStartDistance.length; i++) {
			if (busStopNearStartDistance[i] < min) {
				index = i;
				min = busStopNearStartDistance[i];
			}
		}

		// 始点のバス停
		BusStop busStopStart = busStopNearStart[index];

		// 使用するルート
		BusRoute mainRoute = routes[index];

		BusPlan[] plans = dh.get(busStopStart.getId(), busStopGoal.getId(), mainRoute, baseTime);

		if (plans.length > 0) {
			return plans[0];
		} else {
			return null;
		}
	}
}
